const pup = require('puppeteer');
const fs = require('fs');
const nodemailer = require('nodemailer');

async function run () {
    return new Promise(async (resolve, reject) => {
        const browser = await pup.launch();
        const page = await browser.newPage();
        await page.goto("https://ogloszenia.trojmiasto.pl/nieruchomosci-mam-do-wynajecia/mieszkanie/gdansk/ai,900_3000,fi,1,ri,2_,o1,1.html");

        let titles = await page.evaluate(() => {
            let results = [];
            let items = document.querySelectorAll('a.list__item__content__title__name.link');
            items.forEach( (item) => {
                results.push({
                    title: item.getAttribute("title"),
                    url: item.getAttribute('href')
                })
            })
            return results;
        })

        browser.close();
        return resolve(titles[0]);
    })
}

// async function run2 () {
//     return new Promise(async (resolve, reject) => {
//         const browser = await pup.launch();
//         const page = await browser.newPage();
//         await page.goto("https://www.olx.pl/d/nieruchomosci/mieszkania/wynajem/gdansk/?search%5Bprivate_business%5D=private&search%5Border%5D=created_at:desc&search%5Bfilter_float_price:from%5D=900&search%5Bfilter_float_price:to%5D=3000&search%5Bfilter_enum_rooms%5D%5B0%5D=two&search%5Bfilter_enum_rooms%5D%5B1%5D=three&search%5Bfilter_enum_rooms%5D%5B2%5D=four");
//
//         let titles = await page.evaluate(() => {
//             let results = [];
//             let items = document.querySelectorAll("h6");
//             items.forEach(item => {
//                 console.log(item.innerText)
//             })
//             // let items = document.evaluate("//div[@type='list']/div/h6", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
//             return results;
//         })
//
//         browser.close();
//         return resolve(titles[0]);
//     })
// }
// run().then(console.log)
const transport = nodemailer.createTransport({
    host: "poczta.o2.pl",
    port: 465,
    secure: true,
    ssl: true,
    requireTLS: true,
    auth: {
        user: "notify_krystian",
        pass: "Ha$lo123"
    }
});

let mailOptions = {
    from: 'notify_krystian@o2.pl',
    to: 'maurasey@outlook.com',
    subject: 'Nowe ogloszenie',
    text: "",
    html: ""
}


fs.readFile('newestTrojmiasto.txt', 'utf8', (err, data) => {
    if(err) {
        console.log(err);
        return;
    }
    run().then((item) => {
        if(item.title !== data) {
            console.log("JEST NOWE OGLOSZENIE");
            console.log(item);
            mailOptions.text = item.title + "<br>" + item.url;
            mailOptions.html = item.title + "<br>" + "<a href='" + item.url + "'> " + item.url + "</a>";
            transport.sendMail(mailOptions).then(() => console.log("Wyslano"));
            // ToDo implementacja wysylania maila
            fs.writeFile('newestTrojmiasto.txt', item.title, err => {
                if(err){
                    console.log(err);
                }
            })
        }
    })
})

// run2().then(console.log);